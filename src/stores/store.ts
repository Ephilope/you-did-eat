import { createPinia, defineStore } from 'pinia'

const pinia = createPinia()

export const storeAdding = defineStore('Calories',{
  state: () => ({
    totalcal: 0,
    cal: 0,
    food: '',
    ptitList: [''],
    dejList: [''],
    dinList: [''],
    goutList: [''],
    grignList: ['']
  }),
  getters: {
    getCal: (state) => state.totalcal,
    getPtitList: (state) => state.ptitList,
    getDejList: (state) => state.dejList,
    getDinList: (state) => state.dinList,
    getGoutList: (state) => state.goutList,
    getGrignList: (state) => state.grignList
  },
  actions: {
    addCal(cal: number) {
      this.totalcal += cal
    },
    addPtitList(food:string ) {
      this.ptitList.push(food)
    },
    addDejList(food:string ) {
      this.dejList.push(food)
    },
    addDinList(food:string ) {
      this.dinList.push(food)
    },
    addGoutList(food:string ) {
      this.goutList.push(food)
    },
    addGrignList(food:string ) {
      this.grignList.push(food)
    }
  }
})
export default storeAdding